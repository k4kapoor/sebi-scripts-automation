package org.edreams.sebi.automation.conf;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Getter
public class MembershipConfig {
    @Value("${active.card:ACTIVE}")
    private String activeCard;

    @Value("${expired.card:EXPIRED}")
    private String expiredCard;

    @Value("${activated.membership:ACTIVATED}")
    private String activatedMembership;

    @Value("${membership.renewal.status:ENABLED}")
    private String membershipRenewalStatus;

    @Value("${active.card.count:5}")
    private String activeCardCount;

    @Value("${expired.card.count:5}")
    private String expiredCardCount;

    @Value("${data.folder.name:countries}")
    private String dataFolderName;

    @Value("${sql.union:UNION}")
    private String sqlUnion;

    //-- Configurable fields, depends on when you run  -->
    @Value("${membership.expiration.date.from}")
    private String membershipExpirationDateFrom;

    @Value("${membership.expiration.date.to}")
    private String membershipExpirationDateTo;

    @Value("${active.card.expiry}")
    private String activeCardExpiry;

    @Value("${expired.card.expiry.from}")
    private String expiredCardExpiryFrom;

    @Value("${expired.card.expiry.to}")
    private String expiredCardExpiryTo;

}
