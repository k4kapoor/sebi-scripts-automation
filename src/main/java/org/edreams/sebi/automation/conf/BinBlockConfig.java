package org.edreams.sebi.automation.conf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BinBlockConfig {
    @Value("${bin.block.file.insert.sql:01_insert_bin_ranges_blocked.sql}")
    private String binBlockFileInsertSQL;

    @Value("${bin.block.file.rollback.sql:R1_insert_bin_ranges_blocked.sql}")
    private String binBlockFileRollbackSQL;

    @Value("${bin.block.data.folder:bin-block}")
    private String binBlockDataFolder;

    @Value("${bin.block.data.filename:input-file.txt}")
    private String binBlockDataFile;

    public String getBinBlockFileInsertSQL() {
        return binBlockFileInsertSQL;
    }

    public String getBinBlockFileRollbackSQL() {
        return binBlockFileRollbackSQL;
    }

    public String getBinBlockDataFolder() {
        return binBlockDataFolder;
    }

    public String getBinBlockDataFile() {
        return binBlockDataFile;
    }

}
