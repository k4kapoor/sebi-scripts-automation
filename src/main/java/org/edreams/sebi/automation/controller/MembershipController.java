package org.edreams.sebi.automation.controller;

import org.edreams.sebi.automation.model.BinInfo;
import org.edreams.sebi.automation.processor.BinBlockRangeProcessor;
import org.edreams.sebi.automation.processor.SQLDataProcessor;
import org.edreams.sebi.automation.processor.MergeFilesProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.websocket.server.PathParam;
import java.io.IOException;
import java.util.List;

@RestController
public class MembershipController {
    private final static Logger LOGGER = LoggerFactory.getLogger(MembershipController.class);

    @Autowired
    private SQLDataProcessor SQLDataProcessor;
    @Autowired
    private MergeFilesProcessor mergeFilesProcessor;

    @Autowired
    private BinBlockRangeProcessor binBlockRangeProcessor;

    @GetMapping(value = "/generate-bin")
    ResponseEntity<String> fetchDataForCC() {
        try {
            List<BinInfo> binInfoList = SQLDataProcessor.readTSVFilesAndPrepareData();
            return SQLDataProcessor.generateSQLUnionStatement(binInfoList);
        } catch (IOException e) {
            LOGGER.error("Error in reading the files");
            return ResponseEntity.internalServerError().build();
        }
    }

    @GetMapping(value = "/block-bin")
    ResponseEntity<String> generateInsertAndRollbackScripts() throws IOException {
        return binBlockRangeProcessor.generateInsertAndRollbackScripts();
    }

    @GetMapping(value = "/merge-csv")
    ResponseEntity<String> mergeCSVResultIntoSingleCSV(@PathParam("${directory}" ) String directory) throws IOException {
        return mergeFilesProcessor.mergeCSVResultIntoSingleCSV(directory);
    }


}
