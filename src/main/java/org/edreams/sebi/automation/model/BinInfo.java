package org.edreams.sebi.automation.model;

public class BinInfo {
    private String bank;
    private String country;
    private String ccBrand;
    private String binFrom;
    private String binTo;

    public String getBank() {
        return bank;
    }

    public void setBank(String bank) {
        this.bank = bank;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCcBrand() {
        return ccBrand;
    }

    public void setCcBrand(String ccBrand) {
        this.ccBrand = ccBrand;
    }

    public String getBinFrom() {
        return binFrom;
    }

    public void setBinFrom(String binFrom) {
        this.binFrom = binFrom;
    }

    public String getBinTo() {
        return binTo;
    }

    public void setBinTo(String binTo) {
        this.binTo = binTo;
    }

}
