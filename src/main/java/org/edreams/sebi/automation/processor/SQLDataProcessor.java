package org.edreams.sebi.automation.processor;

import org.edreams.sebi.automation.conf.MembershipConfig;
import org.edreams.sebi.automation.model.BinInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class SQLDataProcessor {
    private final static Logger LOGGER = LoggerFactory.getLogger(SQLDataProcessor.class);
    @Autowired
    private MembershipConfig config;
    @Autowired
    ResourcePatternResolver resourceResolver;

    private static final String META_HEADER = "ALTER SESSION SET NLS_DATE_FORMAT = 'YYYY-MM-DD HH24:MI:SS';\n" +
            "set heading off\n" +
            "set feedback off\n" +
            "set linesize 300\n";
    private static final String REPORT_FILE_N = "SPOOL REPORT_%s.TXT\n";
    private static final String SPOOL_OFF = "SPOOL OFF\n";
    private static final String SELECT_FIELDS = "SELECT DISTINCT\n" +
            "'\"'\n" +
            "|| '%s'\n" +
            "|| '\";\"'\n" +
            "|| gm.id\n" +
            "|| '\";\"'\n" +
            "|| gm.timestamp\n" +
            "|| '\";\"'\n" +
            "|| gm.member_account_id\n" +
            "|| '\";\"'\n" +
            "|| est.collection_entity_id\n" +
            "|| '\";\"'\n" +
            "|| est.recurring_collection_id\n" +
            "|| '\";\"'\n" +
            "|| '%s'\n" +
            "|| '\";\"'\n" +
            "|| '%s'\n" +
            "|| '\";\"'\n" +
            "|| '%s'\n" +
            "|| '\";'";
    private static final String QUERY_WITH_JOINS = " FROM MEMBERSHIP_OWN.GE_MEMBERSHIP GM\n" +
            "INNER JOIN MEMBERSHIP_OWN.GE_MEMBERSHIP_RECURRING GMR ON GM.ID = GMR.MEMBERSHIP_ID\n" +
            "INNER JOIN LAUNCH.ED_SALES_ITEMS EST ON EST.RECURRING_COLLECTION_ID = GMR.RECURRING_ID\n" +
            "INNER JOIN LAUNCH.ED_CREDIT_CARDS ECC ON ECC.ID = EST.COLLECTION_ENTITY_ID\n" +
            "WHERE GM.STATUS = '%s' AND GM.AUTO_RENEWAL = '%s'\n" +
            "AND GM.EXPIRATION_DATE BETWEEN TO_DATE('%s', 'DD/MM/RR HH24:MI:SS') AND TO_DATE('%s', 'DD/MM/RR HH24:MI:SS')\n" +
            "AND COALESCE(GM.RENEWAL_PRICE, 1) <> 0\n" +
            "AND GMR.RECURRING_ID IS NOT NULL\n" +
            "AND GMR.RECURRING_ID <> 'NON_RECURRING'\n";
    private static final String CARD_EXPIRATION_DATE_START_CONDITION =  "--AND TO_DATE(ECC.EXPIRATION_DATE, 'MMRR') >= TO_DATE('%s', 'DD/MM/RR')\n";
    private static final String EXPIRATION_DATE_END_CONDITION = "--AND TO_DATE(ECC.EXPIRATION_DATE, 'MMRR') < TO_DATE('%s', 'DD/MM/RR')\n";
    private static final String ORDER_BY =         "AND ECC.BIN BETWEEN %s AND %s\n" +
            "ORDER BY GM.TIMESTAMP DESC FETCH FIRST %s ROWS ONLY";

    public List<BinInfo> processAndSave(File csvFile) {
        LOGGER.info("Processing the file " + csvFile.getName());
        try {
            return new BufferedReader(new FileReader(csvFile))
                    .lines()
                    .skip(1)
                    .map(s -> {
                        BinInfo bin = new BinInfo();
                        String[] binInfo = s.split("\\t+");
                        if(binInfo.length>0) {
                            bin.setBank(binInfo[0]);
                            bin.setCountry(binInfo[1]);
                            bin.setCcBrand(binInfo[2]);
                            bin.setBinFrom(binInfo[binInfo.length-2]);
                            bin.setBinTo(binInfo[binInfo.length-1]);
                        }
                        return bin;
                    })
                    .collect(Collectors.toList());
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        }
        return Collections.EMPTY_LIST;
    }


    public List<BinInfo> readTSVFilesAndPrepareData() throws IOException {
        List<BinInfo> binInfoList = new ArrayList<>();
        Resource[] allFileResources = resourceResolver.getResources("classpath:" + config.getDataFolderName() +"/*.tsv");
        for (Resource binFile: allFileResources) {
            binInfoList.addAll(processAndSave(binFile.getFile()));
        }
        return binInfoList;
    }

    public ResponseEntity<String> generateSQLUnionStatement(List<BinInfo> binInfoList) {
        StringBuilder sqlBuilder = new StringBuilder();
        sqlBuilder.append(META_HEADER);
        for (int i = 0; i < binInfoList.size(); i++) {
            sqlBuilder.append(String.format(REPORT_FILE_N, (i+1)));
            sqlBuilder.append(createSingleSpoolQueryPerBinRange(binInfoList.get(i)));
            sqlBuilder.append(SPOOL_OFF);
        }
        return ResponseEntity.ok(sqlBuilder.toString());
    }

    private String createSingleSpoolQueryPerBinRange(BinInfo binInfo) {
        String activeMembershipSelects = String.format(SELECT_FIELDS, config.getActiveCard(),
                binInfo.getBank(), binInfo.getCcBrand(), binInfo.getCountry());
        String expiredMembershipSelects = String.format(SELECT_FIELDS, config.getExpiredCard(),
                binInfo.getBank(), binInfo.getCcBrand(), binInfo.getCountry());
        String activeCards = String.format(QUERY_WITH_JOINS + CARD_EXPIRATION_DATE_START_CONDITION + ORDER_BY,
                config.getActivatedMembership(), config.getMembershipRenewalStatus(),
                config.getMembershipExpirationDateFrom(), config.getMembershipExpirationDateTo(),
                config.getActiveCardExpiry(),
                binInfo.getBinFrom(), binInfo.getBinTo(),
                config.getActiveCardCount()
                );
        String expiredCards = String.format(QUERY_WITH_JOINS + CARD_EXPIRATION_DATE_START_CONDITION + EXPIRATION_DATE_END_CONDITION + ORDER_BY,
                config.getActivatedMembership(), config.getMembershipRenewalStatus(),
                config.getMembershipExpirationDateFrom(), config.getMembershipExpirationDateTo(),
                config.getExpiredCardExpiryFrom(), config.getExpiredCardExpiryTo(),
                binInfo.getBinFrom(), binInfo.getBinTo(),
                config.getExpiredCardCount()
        );
        return "(" + activeMembershipSelects + System.lineSeparator()
                + activeCards + ") " + config.getSqlUnion() + System.lineSeparator() +
                "(" + expiredMembershipSelects + System.lineSeparator()
                + expiredCards + ");" + System.lineSeparator();
    }

}
