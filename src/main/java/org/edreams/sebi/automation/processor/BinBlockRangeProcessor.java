package org.edreams.sebi.automation.processor;

import org.edreams.sebi.automation.conf.BinBlockConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Component
public class BinBlockRangeProcessor {
    private static final String INSERT_SQL = "INSERT INTO BIN_RANGES_BLOCKED (RANGE_FROM,RANGE_TO,REASON) SELECT '%s','%s','%s' FROM dual WHERE NOT EXISTS (SELECT * FROM BIN_RANGES_BLOCKED WHERE BIN_RANGES_BLOCKED.RANGE_FROM = '%s' AND BIN_RANGES_BLOCKED.RANGE_TO = '%s')";
    private static final String DELETE_SQL = "delete from bin_ranges_blocked where range_from='%s' and range_to= '%s' and reason= '%s'";

    private static final StringBuilder INSERT_SQL_BUILDER = new StringBuilder();
    private static final StringBuilder DELETE_SQL_BUILDER = new StringBuilder();

    @Autowired
    private BinBlockConfig config;
    @Autowired
    ResourcePatternResolver resourceResolver;

    public ResponseEntity<String> generateInsertAndRollbackScripts() throws IOException {
        Resource[] allFileResources = resourceResolver.getResources("classpath:" + config.getBinBlockDataFolder() +"/" + config.getBinBlockDataFile());
        if(allFileResources.length != 0) {
            for (Resource inputFile: allFileResources) {
                try (BufferedReader reader = Files.newBufferedReader(Paths.get(String.valueOf(inputFile.getFile())))) {
                    reader.lines().forEach(line -> createStatement(line));
                } catch (Exception e) {
                    return ResponseEntity.badRequest().body("Something went wrong \n" + e);
                }
                writeSQLFiles(config.getBinBlockFileInsertSQL(), INSERT_SQL_BUILDER);
                writeSQLFiles(config.getBinBlockFileRollbackSQL(), DELETE_SQL_BUILDER);
            }
        }
        return ResponseEntity.ok().body("The files are generated for insert and rollback");
    }

    private static void writeSQLFiles(String filename, StringBuilder contents) {
        try (FileWriter fileWriter = new FileWriter(filename)) {
            fileWriter.write(String.valueOf(contents));
        } catch (IOException e) {
            System.out.println("Error writing to SQL file: " + e);
        }
    }

    private static void createStatement(String record) {
        String[] keywords = record.split("\\s+");
        String reasonDescription = record.replace(keywords[0], "").replace(keywords[1], "").trim();
        INSERT_SQL_BUILDER.append(String.format(INSERT_SQL, keywords[0], keywords[1], reasonDescription, keywords[0], keywords[1])).append(";\n");
        DELETE_SQL_BUILDER.append(String.format(DELETE_SQL, keywords[0], keywords[1], reasonDescription)).append(";\n");
    }

}
