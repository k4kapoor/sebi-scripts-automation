package org.edreams.sebi.automation.processor;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

@Component
public class MergeFilesProcessor {
    private String generatedFileName = "";

    public ResponseEntity<String> mergeCSVResultIntoSingleCSV(String directory) {
        List<String> mergedRecordList = new LinkedList<>();
        if(directory != null) {
            File directoryFile = new File(directory);
            if(directoryFile.isDirectory()) {
                generatedFileName = directoryFile.getName();
                File[] files = directoryFile.listFiles();
                Arrays.stream(files).forEach(recordFile -> mergedRecordList.addAll(mergeCurrentFile(recordFile)));
                mergeToSingleCSV(directoryFile, mergedRecordList);
                return ResponseEntity.ok("All files are processed and merged into one big file.");
            } else {
                return ResponseEntity.badRequest().body("you provided a wrong file for parameter directory '" + directory + "'");
            }
        }
        return ResponseEntity.badRequest().body("you provided a wrong path parameter value for directory '" + directory + "', Make sure its a valid directory.");
    }

    private List<String> mergeCurrentFile(File recordFile) {
        try {
            return Files.readAllLines(Paths.get(recordFile.getPath()), StandardCharsets.UTF_8);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    private void mergeToSingleCSV(File directoryFile, List<String> mergedRecordList) {
        try {
            Path target = Paths.get(directoryFile + "/" + generatedFileName + "_" + System.currentTimeMillis());
            Files.write(target, mergedRecordList, Charset.forName("UTF-8"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
