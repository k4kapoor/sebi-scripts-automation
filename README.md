# README #

### What is this repository for? ###

There are many instances where we can use [sebi-scripts-automation](https://bitbucket.org/k4kapoor/sebi-scripts-automation/src/master/) to generate SQLs which in normal scenarios is lots of manual copy pasting </br>
* Fetch CC for a specific expiry date range
* Insert/Rollback script for blocking a BIN range

## How do I Generate automated bulk queries for "Fetch CC for a specific expiry date range"
   * Data files should be in the src/main/resources/countries/ in the form of TSV files (as bank name can also contain commas so we avoid CSV) Note; You can have a single file or multiple files , all files under this folder will be processed)
   * Configure all fields in application.properties, specifically dates
   
   
    example
       To run for March Month
        Active Card Expiration condition  >= '01/04/22'
        Expired Card Expiration condition  >= '01/01/22' &&  < '01/03/22'

* Run Application.java
* Invoke API http://localhost:9000/generate-bin
* Generated script is in API response


## How do I Generate new Block BIN range Insert/Rollback script
* Run Application.java
* Copy data in src/main/resource/block-bin/input-file.txt as in the below sample (Three record separated by space)

    
    example:
    5246939999000000000 	5246950000999999999 	INTESA SANPAOLO SPA
    4165986999000000000 	4165987300999999999 	UNKNOWN
* Invoke API http://localhost:9000/block-bin
* Files generated in the project root with name 01_insert_bin_ranges_blocked.sql and R01_insert_bin_ranges_blocked.sql(Configurable in application.properties)


### Who do I talk to? ###

* gaurav.kapoor or help-sebi